package com.amdocs.KM.controller;

import com.amdocs.KM.domain.User;
import com.amdocs.KM.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    UserService userService;

    @PostMapping
    public void saveUser(@RequestBody User user){
        userService.addUser(user);
    }

    @GetMapping
    public ResponseEntity<List<User>> getAllUsers(){
        return  new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
    }
}
