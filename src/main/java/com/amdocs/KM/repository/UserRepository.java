package com.amdocs.KM.repository;

import com.amdocs.KM.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository  extends CrudRepository<User,Integer> {
}
