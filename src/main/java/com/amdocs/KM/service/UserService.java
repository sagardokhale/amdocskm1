package com.amdocs.KM.service;

import com.amdocs.KM.domain.User;
import com.amdocs.KM.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    public void addUser(User user){
        userRepository.save(user);
    }

    public List<User> getAllUsers(){
        List<User> userList=new ArrayList<>();
        userRepository.findAll().forEach(user->userList.add(user));
        return userList;
    }
}
